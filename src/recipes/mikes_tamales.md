# Tamales

## Yield

Makes about 26

## Ingredients

### Filling

- 1 (8-ounce) package dried corn husks
- 1 pound tomatillos, husked, rinsed
- 4 (3-inch-long) serrano chiles, stemmed, chopped
- 4 large garlic cloves, chopped
- 1 1/2 tablespoons olive oil
- 2 cups low-salt chicken broth
- 4 cups (packed) coarsely shredded cooked chicken (about 1 pound; from purchased rotisserie chicken)
- 2/3 cup chopped fresh cilantro

### Dough

- 1 1/3 cups lard or solid vegetable shortening
- 1 1/2 teaspoons salt (omit if masa mixture contains salt)
- 1 1/2 teaspoons baking powder (omit if masa mixture contains baking powder)
- 4 cups freshly ground masa dough for tamales (34 to 36 ounces), or make masa dough with 31/2 cups masa harina (corn tortilla mix; about 17 ounces) mixed with 2 1/4 cups warm water
- 2 cups (about) low-salt chicken broth

## Preparation

### For filling

1. Place husks in large pot or large bowl; add water to cover.
1. Place heavy plate on husks to keep submerged.
1. Let stand until husks soften, turning occasionally, at least 3 hours and up to 1 day.
1. Preheat broiler.
1. Line heavy baking sheet with foil.
1. Arrange tomatillos on prepared sheet.
1. Broil until tomatillos blacken in spots, turning once, about 5 minutes per side.
1. Transfer tomatillos and any juices on sheet to processor and cool.
1. Add chiles and garlic to processor and blend until smooth puree forms. 
1. Heat oil in medium saucepan over medium-high heat.
1. Add tomatillo puree and boil 5 minutes, stirring often.
1. Add broth.
1. Reduce heat to medium; simmer until sauce coats spoon thickly and is reduced to 1 cup, stirring occasionally, about 40 minutes.
1. Season with salt.
1. Mix in chicken and cilantro. (Can be made 1 day ahead. Cover and chill.)

### For dough

1. Using electric mixer, beat lard (with salt and baking powder, if using) in large bowl until fluffy. Beat in fresh masa or masa harina mixture in 4 additions. Reduce speed to low and gradually beat in 1 1/2 cups broth, forming tender dough. If dough seems firm, beat in enough broth, 2 tablespoons at a time, to soften.
1. Fill bottom of pot with steamer insert with enough water (about 2 inches) to reach bottom of insert. Line bottom of insert with some softened corn husks. 
1. Tear 3 large husks into 1/4-inch-wide strips to use as ties and set aside.
1. Open 2 large husks on work surface. Spread 1/4 cup dough in 4-inch square in center of each, leaving 2- to 3-inch plain border at narrow end of husk.
1. Spoon heaping tablespoon filling in strip down center of each dough square.
1. Fold long sides of husk and dough over filling to cover.
1. Fold up narrow end of husk
1.  Tie folded portion with strip of husk to secure, leaving wide end of tamale open.
1.  Stand tamales in steamer basket.
1.  Repeat with more husks, dough, and filling until all filling has been used.
1.  If necessary to keep tamales upright in steamer, insert pieces of crumpled foil between them.
1. Bring water in pot to boil.
1. Cover pot and steam tamales until dough is firm to touch and separates easily from husk, adding more water to pot as necessary, about 45 minutes.
1. Let stand 10 minutes. (Can be made 2 days ahead.
1. Cool 1 hour.
1. Cover and chill.
1. Before serving, re-steam tamales until hot, about 35 minutes.)
