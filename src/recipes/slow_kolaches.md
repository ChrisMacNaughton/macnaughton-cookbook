# Kolaches

## Yield

Serves: about 15 kolaches

## Ingredients

### For the dough:

- 2¼ tsp active dry yeast
- ¼ cup water, lukewarm
- 1 cup milk
- 4 tbsp butter
- 2 large eggs
- ⅔ cup sugar
- 1 tsp salt
- 5 cups flour (+/- ½ cup)

### For the filling:

- 8 slices of cheese
- 15 smoked sausage links
- 2 jalapeños, julienned (cut into strips)

## Instructions


Add the yeast and lukewarm water to the bowl of a stand mixer with the dough hook attached. Allow the mixture to sit for 5 minutes, until foamy.

Heat the butter and milk in a small saucepan until the butter has melted and the milk is lukewarm (about 110 degrees Fahrenheit). Remove from heat and add to the yeast mixture. Add the eggs and stir slowly until the eggs are incorporated. Then, add the sugar and salt and stir gently.

Add the flour one cup at a time while stirring the dough. Keep adding flour until the dough becomes workable, but still tacky. This is somewhere between 4½ to 5½ cups of flour total. Adding too much flour will cause the dough to be dense, so it is better to slightly underestimate and add more later. Cover the bowl with plastic wrap and allow the dough to rise until doubled in size, between one and two hours.

After it has doubled in size, punch the dough down then cover and let rise in the refrigerator at least 5 hours, preferably overnight.

Remove the dough from the refrigerator and divide into 15 equal-sized balls about 2.75 ounces each (around 2 inches in diameter). Place the balls of dough on a lined baking sheet. Flatten the balls into ovals and make a long indention into the center using your thumb for the filling. If the dough is too cold and difficult to work with, cover and allow the dough to rest for 10 to 20 minutes.

Place half a slice of cheese into the indention, then top with a sausage link and some julienned jalapeño. Fold the dough around the fillings, pinching the edges together. Place on the lined baking sheet, seam side down. Repeat with the remaining dough.

Cover and allow the dough to rest while preheating the oven to 375 degrees Fahrenheit, or for about 20 minutes. This will bring the dough close to room temperature before baking.

Bake for about 20 to 25 minutes, until golden brown.

Let the kolaches cool before serving; the filling will be very hot.