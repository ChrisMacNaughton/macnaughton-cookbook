# MacNaughton Cookbook

This cookbook is the result of combining multiple generations of recipes into a consolidated location.

To navigate this cookbook, it is recommended to use the navigation area on the left.